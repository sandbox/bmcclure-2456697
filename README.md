Pardot Search module for Drupal
=====================

Drupal 7.x module to redirect search submissions to the Pardot service 
before coming back to the results page.