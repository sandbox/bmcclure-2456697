<?php
/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 3/20/2015
 * Time: 5:02 PM
 */

function pardot_search_admin_form($form, &$form_state) {
  $form['pardot_search_post_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Post URL'),
    '#default_value' => variable_get('pardot_search_post_url', ''),
    '#size' => 60,
    '#maxlength' => 255,
    '#description' => t("The Post URL provided by Pardot for this Site Search"),
    '#required' => TRUE,
  );

  $form['pardot_search_query_variable'] = array(
    '#type' => 'textfield',
    '#title' => t('Query Variable'),
    '#default_value' => variable_get('pardot_search_query_variable', 'terms'),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t("The name of the query variable as configured at Pardot Search"),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

